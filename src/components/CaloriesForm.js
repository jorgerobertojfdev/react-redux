import React from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';

const initialState = () => ({
  form: {
    date: '',
    time: '',
    text: '',
    calories: '',
    id: ''
  }
});

export default class CaloriesForm extends React.Component {
  constructor(props) {
    super(props);

    this.sendCalories = this.sendCalories.bind(this);
    this.updateStateForm = this.updateStateForm.bind(this);

    this.state = initialState();
  }

  sendCalories(e) {
    e.preventDefault();

    this.props.submit(this.state.form);

    this.setState(initialState());
  }


  updateStateForm(event) {
    const name = event.target.name;
    const value = event.target.value;
    const newState = Object.assign(this.state.form, { [name]: value, id: Date.now() });

    this.setState(newState);
  }

  render() {
    return (
      <Form onSubmit={this.sendCalories}>
        <Form.Field>
          <label>Date</label>
          <input type="date" name="date" placeholder='ex: 29/04/2017' value={this.state.form.date} onChange={this.updateStateForm}  required />
        </Form.Field>
        <Form.Field>
          <label>Time</label>
          <input type="time" name="time" placeholder='Ex: 09:55' value={this.state.form.time} onChange={this.updateStateForm}  required />
        </Form.Field>
        <Form.Field>
          <label>Text</label>
          <textarea name="text" name="text" placeholder="Text" cols="30" rows="10" value={this.state.form.text} onChange={this.updateStateForm}></textarea>
        </Form.Field>
        <Form.Field>
          <label>Number of Calories</label>
          <input type="number" name="calories" placeholder="Ex: 235" value={this.state.form.calories} onChange={this.updateStateForm} required />
        </Form.Field>
        <Button type='submit'>Enviar</Button>
      </Form>
    );
  }
}
