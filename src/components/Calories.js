import React from 'react';
import { Grid, Segment, Container, Header } from 'semantic-ui-react'
import { connect } from 'react-redux';

import { addCalorie } from '../actions/index.js';
import CaloriesForm from './CaloriesForm';
import CaloriesTable from './CaloriesTable';

class Calories extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <Container>
        <Grid columns='equal'>
          <Grid.Row>
            <Grid.Column>
              <Segment>
                <Header as="h2">Quantas Calorias você comeu ?</Header>
                <CaloriesForm submit={this.props.addCalories}></CaloriesForm>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Segment>
                <Header as="h2">Calorias</Header>
                <CaloriesTable expectedCalories="200" data={this.props.calories}></CaloriesTable>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    calories: state.calories
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addCalories: (data) => {
      dispatch(addCalorie(data))
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Calories);
