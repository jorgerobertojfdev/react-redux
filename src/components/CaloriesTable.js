import React from 'react';
import { Icon, Table  } from 'semantic-ui-react'

export default class CaloriesTable extends React.Component {
 render() {
   return (
     <Table celled>
       <Table.Header>
         <Table.Row>
           <Table.HeaderCell>Date</Table.HeaderCell>
           <Table.HeaderCell>Time</Table.HeaderCell>
           <Table.HeaderCell>Text</Table.HeaderCell>
           <Table.HeaderCell>Calories</Table.HeaderCell>
         </Table.Row>
       </Table.Header>

       <Table.Body>
        {this.props.data.map((item) => {
          if (Number(item.calories) <= Number( this.props.expectedCalories)) {
            return (<Table.Row positive key={item.id}>
              <Table.Cell>{item.date}</Table.Cell>
              <Table.Cell>{item.time}</Table.Cell>
              <Table.Cell>{item.text}</Table.Cell>
              <Table.Cell>{item.calories}</Table.Cell>
            </Table.Row>);
          } else {
            return (<Table.Row negative key={item.id}>
              <Table.Cell>{item.date}</Table.Cell>
              <Table.Cell>{item.time}</Table.Cell>
              <Table.Cell>{item.text}</Table.Cell>
              <Table.Cell>{item.calories}</Table.Cell>
            </Table.Row>);
          }
        })}
       </Table.Body>
     </Table>
   );
 }
}
