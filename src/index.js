import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import Calories from './components/Calories';
import store from './store';

class App extends React.Component {
  render() {
    return(
      <Calories></Calories>
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,document.getElementById('app'));
