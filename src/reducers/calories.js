import { addCalorie } from '../actions/index';

export default (state = [], action) => {
  switch (action.type) {
    case 'ADD_CALORIE':
      return state.concat([addCalorie(action)]);
    default:
      return state;
  }
};
