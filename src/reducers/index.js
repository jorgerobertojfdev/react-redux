import { combineReducers } from 'redux';
import calories from './calories.js';

export default combineReducers({
  calories
});
