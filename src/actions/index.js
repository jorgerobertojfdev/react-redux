export const addCalorie = (data) => {
  const { date, time, text, calories, id } = data;
  return { type: 'ADD_CALORIE', date, time, text, calories, id };
};
